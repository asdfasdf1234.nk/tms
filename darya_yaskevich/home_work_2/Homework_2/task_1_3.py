# Set the length of an edge of a cube.
a = 3

# Define variables for the volume of a cube and the lateral surface area.
v = a ** 3
lsa = 4 * a ** 2

print(v, lsa, sep='\n')
