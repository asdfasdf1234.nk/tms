from datetime import datetime

# 1. Напишите функцию, которая возвращает строку: “Hello world!”


def hello_world():
    return "Hello world"


print(hello_world())

# 2. Напишите функцию, которая вычисляет сумму трех чисел
# и возвращает результат в основную ветку программы.


def sum_three(a, b, c):
    return a + b + c


print(sum_three(2, 3, 4))

# 3. Придумайте программу, в которой из одной функции вызывается вторая.
# При этом ни одна из них ничего не возвращает в основную ветку программы,
# обе должны выводить результаты своей работы с помощью функции print().


def func1():
    print('Hello world')
    func2()


def func2():
    print("Lalala")

# 4. Напишите функцию, которая не принимает отрицательные числа.
# и возвращает число наоборот.


n = int(input())


def is_even(n):
    if n >= 0:
        n = str(n)
        n2 = "".join(reversed(n))
        return n2
    else:
        pass


print(is_even(n))

# 5.  Напишите функцию fib(n), которая по данному целому неотрицательному
# n возвращает n-e число Фибоначчи.


def fib(n):
    if n in (1, 2):
        return 1
    return fib(n - 1) + fib(n - 2)


print(fib(17))

# 6. Напишите функцию, которая проверяет на то,
# является ли строка палиндромом или нет.


def palindrome(str1):
    reversed_str = "".join(reversed(str1))
    if reversed_str == str1:
        return print("Congrats! This is palindrome")
    else:
        print("Sorry, this is not a palindrome")


palindrome("lalala")

# 7. У вас интернет магазин, надо написать функцию
# которая проверяет что введен правильный купон и он еще действителен


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    date_formatter = "%B %d, %Y"
    date1 = datetime.strptime(current_date, date_formatter)
    date2 = datetime.strptime(expiration_date, date_formatter)
    if (entered_code == correct_code) and (date1 <= date2):
        return True
    else:
        return False


print(check_coupon("123", "123", "July 9, 2015", "July 9, 2015"))
print(check_coupon("123", "123", "July 9, 2015", "July 2, 2015"))

# 8. Фильтр. Функция принимает на вход список, проверяет есть ли эти
# элементы в списке exclude, если есть удаляет их и возвращает
# список с оставшимися элементами

a = ["Mallard", "Hook Bill", "African", "Crested", "Pilgrim", "Toulouse",
     "Blue Swedish"]
# a = ["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
# a = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]


def remove_some_items(a):
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
    return filter(lambda x: x not in exclude, a)


print(list(remove_some_items(a)))
