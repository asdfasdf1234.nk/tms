import random

# Task 1: Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
str1 = 'www.my_site.com#about'
print('Task 1:', str1.replace('#', '/'))

# Task 2: В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
str2 = 'Ivanou Ivan'
str2_upd = str2.split()[::-1]
print('Task 2:', ' '.join(str2_upd))

# Task 3: Напишите программу которая удаляет пробел в начале строки
str3 = ' test'
print('Task 3:', str3.lstrip())

# Task 4: Напишите программу которая удаляет пробел в конце строки
str4 = 'test '
print('Task 4:', str4.rstrip())

# Task 5: a=10, b=23, поменять значения местами,
# чтобы в переменную “a” было записано значение “23”, в “b” - значение “-10”
a = 10
b = 23
a, b = b, a * -1
print('Task 5:', 'a is', a, 'b is', b)

# Task 6: значение переменной “a” увеличить в 3 раза, а значение “b”
# уменьшить на 3
a *= 3
b -= 3
print('Task 6:', 'a is', a, 'b is', b)

# Task 7: преобразовать значение “a” из целочисленного в число с плавающей
# точкой (float), а значение в переменной “b” в строку
a = float(a)
b = str(b)
print('Task 7:', 'a is', a, 'b is', b)

# Task 8: Разделить значение в переменной “a” на 11 и вывести результат с
# точностью 3 знака после запятой
print('Task 8:', round(a / 11, 3))

# Task 9: Преобразовать значение переменной “b” в число с плавающей точкой и
# записать в переменную “c”. Возвести полученное число в 3-ю степень
c = float(b)
print('Task 9:', c ** 3)

# Task 10: Получить случайное число, кратное 3-м
random_digit = random.randrange(3, 9999, 3)
print('Task 10:', random_digit)
# не придумала как сделать это не задавая границу

# Task 11: Получить квадратный корень из 100 и возвести в 4 степень
square_root = int((100 ** 0.5) ** 4)
print('Task 11:', square_root)

# Task 12: Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
# “Hi guysHi guysHi guysToday”
str5 = 'Hi guys'
result = str5 * 3 + 'Today'
print('Task 12:', result)

# Task 13: Получить длину строки из предыдущего задания
print('Task 13:', len(result))

# Task 14: Взять предыдущую строку и вывести слово “Today” в прямом и обратном
# порядке
result = 'Hi guysHi guysHi guysToday'
print('Task 14:', 'Direct order:', result, ',', 'Reverse order:',
      result[:result.index('Today')] + (result[result.index('Today'):
                                               len(result)][::-1]))

# Task 15: “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и
# обратном порядке
remove_spaces = result.replace(' ', '')
each_2nd = remove_spaces[1::2]
print('Task 15:', 'Direct order:', each_2nd, 'Reverse order:', each_2nd[::-1])

# Task 16: Используя форматирования подставить результаты из задания 10 и 11 в
# следующую строку “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>,
# <в обратном>”
print('Task 16:', 'Task 10:', str(random_digit), str(random_digit)[::-1],
      'Task 11:', str(square_root), str(square_root)[::-1])

# Task 17: Есть строка: “my name is name”. Напечатайте ее, но вместо 2ого
# “name” вставьте ваше имя.
s3 = 'my name is name'
result1 = s3.replace(" is name", " is Elena")
print('Task 17:', result1)

# Task 18: Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
# б) все слова в нижнем регистре
# в) все слова в верхнем регистре
print('Task 18:', 'a)', result.title(), 'b)', result.lower(), 'c)',
      result.upper())

# Task 19: Посчитать сколько раз слово “Task” встречается в строке из зад. 12
print('Task 19:', result.count('Task'))
